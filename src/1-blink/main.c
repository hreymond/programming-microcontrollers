#include "msp430.h"

// This code is required for the tutorial;
// I won't get into the details but do not remove this function
int setup() {

    WDTCTL = WDTPW | WDTHOLD; // Disable watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Unlock GPIO pins
}

int main() {
    // Do not remove this function
    setup();

    // MISSION 1.1 Code goes here

    // Main loop
    while(1) {
        // MISSION 1.2 Code goes here
    }
}
