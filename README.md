---
pandoc-latex-admonition:
# order is important
  - color: firebrick
    classes: [your_turn]
  - color: navy
    classes: [info]
  - color: gray
    classes: [admonition]
---

# Bare-Metal Programming for Microcontrollers : A Hands-On Tutorial

This tutorial aims to explain the basics of microcontrollers programming.

## Requirements

- libpython2.7 (Unfortunately :/)
- git
- tar
- make

On debian-based machines : `sudo apt install libpython2.7 git tar make`

## Our beautiful microcontroller : The msp430fr5969

Here is our beast, the `msp430fr5969` ! The `msp430fr5969` (`msp` for short) is a low-power microcontroller. It is designed to be embedded in an IoT device -- e.g., your TV remote, your thermostat -- yet it still provides various features via its internal modules. In this tutorial, we only focus on three modules, highlighted on Figure \ref{fig:func-description} :

- The CPU, which runs our code,
- The I/O ports, which allow us to interact with external peripherals, such as a led or a button,
- The eUSCI module, which allow us to communicate with the external world via UART (a serial communication protocol).

![The msp430 functional diagram\label{fig:func-description}](figures/msp430_functional_diagram.jpeg)

For this tutorial, we will need the following documentation:

- The [MSP430FR59XX Family User's Guide](https://www.ti.com/lit/ug/slau367p/slau367p.pdf), our bible,
- The [MSP430FR5969 Datasheet](https://www.ti.com/lit/ds/symlink/msp430fr5969.pdf) which contains device-specific information.

## 1. Blinking a led, the microcontroller "Hello world !"

Your first mission, should you choose to accept it, will be to blink a led. Blinking a led is the microcontroller equivalent of the famous "Hello World !" program.
Indeed, as you've got no `libc` nor any OS, if you want a  `printf`, you'll need implement it, as well as all the drivers required (cf Mission 3).

However, before getting into any `printf` or blinking considerations, lets try to simply compile and flash a program.

🎯 Goals 🎯
- Run a program on the microcontroller
- Understand the microcontroller memory layout
- Read and extract information from the microcontroller (UC) datasheet
- Interact with a simple peripheral


### Goal 1.0 : Getting stuff ready

Programming microcontroller requires at least two crucial tools :

- A crosscompiler, to compile our program for the MSP430 CPU target, and its wonderful ISA
- A flash tool, to send our program over to the MSP430

Fortunately, your host prepared an install script to install all this mess and configure everything.

::: {.your_turn} :::
- Clone the repository : `git clone https://gitlab.inria.fr/hreymond/programming-microcontrollers.git`
- `cd programming-microcontrollers`
- Run the setup script : `chmod +x setup.sh; ./setup.sh`
- The setup script should have created a `CROSS_COMPILERS` and `DSLITE` folder, as well as a `env.sh` script.
- Add the msp430 compiler to your PATH : `source ./env.sh`.

Now, try to compile your first MSP430 program :

- `cd src/1-blink`
- `make` should produce a binary file `main.o` and a elf file `main.elf`
- `make flash` should flash the program on the MSP430.

::::::::::::::::::::

And now you're ready to start ! But first, a bit of information...

### 💡 A few words on microcontrollers and memory 💡

On modern computers, the developper does not have to worry about memory. Memory is (virtually) infinite,  and the OS is abstracting all the gory details of memory allocation and management (`malloc` and so on).

On small microcontrollers, you're on your own: there is no OS and no standard library. Most of the time, there is no such thing as ``virtual memory'', as there is no memory management unit (MMU) to translate address from virtual to physical addresses : every address you manipulate is a physical address. If you're lucky, you will have access to a memory protection unit (MPU) to disable write, read or execute on some memory range.

Despite the lack of memory abstraction and memory management, there are some good news : interacting with peripheral is simple -- just write some bytes at a given address, and your led magically blinks. Indeed, MCU use the same address space to address both main memory and I/O devices. In the jargon, it is called *memory-mapped I/O*.

Here is the (simplified) memory layout of the MSP430.

![MSP430FR5969 Memory Map (Simplified)](figures/memory_map.png)

Microcontroller have at least two type of memories :
- A non-volatile memory to store program code
- A volatile memory (refer to as `RAM`) to store program data (globals, stack, ...)

They also reserve a portion of memory for the *interrupt vectors*, which contain pointer to function that need to be called on an interruption. Most interrupts vectors are used to trigger callbacks when a peripheral wants to communicate with the CPU. The first interrupt vector, called `reset`, contains the address to the program entry point.

::: {.info} ::
❓ Do you known what VMA and LMA mean, when displaying sections of a elf binary ? ❓

```
x86 binary :
> objdump -h a.out
Idx Name  Taille  VMA      LMA     Off fich Algn
1 .text  d298    0406210  0406210  6210  2**4
2 .data  0004    0404000  0404000  3000  2**0

msp430 binary :
> objdump -h a.out
Idx Name  Taille  VMA      LMA     Off fich Algn
1 .text   0078    04404    04404  000000dc  2**1
2 .data   0002    01c00    04400  000000d4  2**1
```
::::::::::::::

### Mission 1.1 : Like a Lighthouse in the dark


Each peripheral has a set of *registers*, and communicating with peripherals is achieved by writing given values in the peripheral registers. As an example, the GPIO peripheral has a register, called P1DIR, to set the direction of the pins (INPUT or OUTPUT). Those registers are directly accessible from the CPU address space, so configuring a register is as simple as writing a given location in memory.

Well, let's find the right address to light our led ! To simplify development, the microcontroller manufacturer provide header files that define each register as a macro.

```c
// msp430.h
// This macro inside msp430.h allow to access the P1DIR register with P1DIR = VALUE
#define P1DIR *(volatile int *(0x0204))

// main.c
// Put the value 0xFF in the register P1DIR
P1DIR = 0xFF;
```

::: {.your_turn} :::
The msp430 launchpad has a built-in led, linked to the pin P1.O (Port 1, Pin 0). Setting the pin P1.0 to high state should light the led. Try it !

- Ressources : Chapter 12 of the User's Guide
  - 12.2.2 PXOUT (Output registers)
  - 12.2.3 PXDIR (Direction registers)

Note : the `msp430.h` header also defines macros for bits -- `BIT0` = 0x01, `BIT2` = 0x02, `BIT3` = 0x04 and so on...
::::::::::::::::

### Mission 1.2 : Now let's blink !

Now that you know how to light a led, you can easily make it blink.

::: {.your_turn } :::
For this, you'll need to modify the main loop and you'll need to use the `__delay_cycles(<DELAY>)` function, that waits a given delay (in us, as the msp runs at 1MHz).

Make that led blink !
:::::::::::::::::::::

## 2. Push button

By this point, you should have a led blinking !

Now let's try to interact with a push button. We aim to modify the program state as soon as the button state changes (pushed/released). Monitoring events such as a button push can be achieved through two ways (at least that I know of !) : *active polling* or *interrupts*.

*Active polling* consist in regularly sensing an input (here our button) to detect any changes. On the opposite, *Interrupts* rely on hardware to signal input change to the CPU.

In this tutorial, we will first implement *active-polling* to detect a button press, and then switch to *interrupts*. The button on the msp430 launchpad is linked to the pin P1.1.


🎯 Goals 🎯
- Understanding the concept of Pull-Up/Pull-down resistor
- Setting a interrupt
- A word on Low-Power Modes

::: {.info} :::
💡 About Pullup/Pulldown resistor 💡

![The button (S2) is connects the msp430 to the ground](figures/switch_wiring.png)

What is the logical value at P1.1 :
- if S2 is closed ?
- if S2 is open ?

![A Pullup resistor is added to P1.1](figures/pullup.png)

With the new PullUp resistor, what is the logical value at P1.1 :
- if S2 is closed ?
- if S2 is open ?

:::::::::::::::

### Mission 2.2: Active polling

::: {.your_turn} :::
For this missionn modify the code in `src/2-button\`.

Before your main loop, define the pin P1.1 as input, and enable its internal pull-up resistor.

Within your main loop, update the led status given the button status (P1.1): button pressed = led on, button released = led off.

- Ressources : Chapter 12 of the User's Guide
  - 12.2.1 PXIN (Input registers)
  - 12.2.3 PXDIR (Direction registers)
  - 12.2.4 PXREN (Pullup or Pulldown registers)
:::::::::::::::::::::

### Mission 2.3: Interrupts


::: {.your_turn} :::
Now, you must toggle the led (on/off) every time the button is pressed, using interrupts

- Ressources : Chapter 12 of the User's Guide
  - 12.2.6 Port Interrupts & PXIFG
  - 12.2.6.2 and 12.2.6.3 PXIE and PXIES
- To enable interrupts globally :

```c
__bis_SR_register(GIE); // Set the Global Interrupt Enable
```

Remember to always clear the interrupt flag after you've handle the interrupt, otherwise your interrupt routine will be called directly after.
:::::::::::::::::::::


#### 💡 A few words on low-power modes 💡

Microcontrollers are often used in embedded devices powered by battery. As you might imagine, energy-efficiency is a key requirement for such devices.


From now on, we haven't really been energy efficients : either we actively sample the button status in an infinite loop, or wait for an interrupt...while running an infinite loop. To spare some energy, microcontrollers like the msp430 can enter deep sleep modes, by deactivating the CPU when it is not needed.


By entering a low-power mode, the msp430 is able to divide its current consumption by approximately 1000 (170uA active, 0.2uA in deepsleep)

```c
while(1)
{
  __bis_SR_register(LPM4_bits | GIE);  // Go to sleep and activate interrupts
  // Button has been pressed !
}

__interrupt void Port_1(void)
{
  .....
  __bic_SR_register_on_exit(LPM4_bits);     // Exit deepsleep
}
```

## 3. Communicating with the external world

🎯 Goals 🎯
- Interact with a complex peripheral
- Understand pin multiplexing

I didn't had the time to write explaination here, sorry...

::: {.your_turn} :::
- Install picocom, and launch it `sudo picocom /dev/ttyACM1`
- Configure the P2.0 and P2.1 in UCA0TX and UCA0RX function
- Write in the UCA0TXBUF to send a character
- Once you've received a character, create function that sends an entire string (make sure to verify that the TX buf is empty before sending a new character)

**Ressources:**
- Chapter 30 of the User's Guide
  - 30.4.6 et 30.4.7 UCA0TXBUF & UCA0RXBUF
  - 30.4.11 UCA0IFG
- Section 6.11 of the msp430 datasheet
  - 6.11.5 Table 6-52 and Port P2 Input/Output Diagram
::::::::::::::::::::

## To go further

::: {.your_turn} :::
A few ideas to go explore further the msp430 capabilities and improve your microcontroller programming skills

- Write a small "echo" program, that sends backs each character that the user send via Uart
  - Normal version : with *active-polling*
  - Difficult version : with *interrupts*
- Explore the (complex !) clock and timer system to send a character every second (Sections 3 and 25 of the msp430 User Guide)
- Use the integrated temperature sensor of the ADC to display the current temperature (Section 34 and 34.2.11)
- Example code are available on [TI Resource Explorer](https://dev.ti.com/tirex/explore/node?devtools=MSP-EXP430FR5969&node=A__AL2dmw21b-kJdwBXB2JdHw__msp430ware__IOGqZri__LATEST)
::::::::::::::::::::

## NOTES :

If you want IntelliSense to work, you must define the `__MSP430FR5969__` symbol in IntelliSense. If you use VSCode, this is done with the config file in `.vscode/c_cpp_properties.json`.
