#include "msp430.h"

// This code is required for the tutorial;
// I won't get into the details but do not remove this function
int setup() {

    WDTCTL = WDTPW | WDTHOLD; // Disable watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Unlock GPIO pins

    // Startup clock system with max DCO setting ~8MHz
    CSCTL0_H = CSKEY >> 8;                    // Unlock clock registers
    CSCTL1 = DCOFSEL_3 | DCORSEL;             // Set DCO to 8MHz
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;     // Set all dividers
    CSCTL0_H = 0;                             // Lock CS registers
    // SMCLK is running at 4Mhz
    // Target baudrate is 9600, divider is 52,083
    // Oversampling mode deactivated (default)
    /* Set software reset */
    UCA0CTLW0 = UCSWRST;
    /* default 8N1, lsb first, uart, input clk SMCLK */
    UCA0CTLW0 |= UCSSEL__SMCLK;
    UCA0BRW = 52;
    UCA0IFG = 0;
    UCA0MCTLW = 0;
    UCA0MCTLW |= UCOS16 | UCBRF_1;
    UCA0STATW = UCLISTEN;
    UCA0CTLW0 &= ~UCSWRST;
}

int main() {
    // Do not remove this function
    setup();

    // Main loop
    while(1) {
    }
}
