#!/bin/bash
# This code is taken from Heptane WCET tool

case "$(uname)" in
    Linux)
        HOST_OS="LINUX"
        ROOT=`dirname "$(readlink -f "$0")"`
    ;;
    Darwin)
        HOST_OS="MACOS"
        ROOT=`pwd -P`
    ;;
    *)
        echo "Found unknown OS. Aborting!"
        exit 1
    ;;
esac

echo "Installing the MSP430 CROSS COMPILER"

OS=${HOST_OS}
ARCH=MSP430
CROSS_COMPILERS_DIR=${ROOT}/CROSS_COMPILERS

CC_DIR=${ARCH}_${OS}

if [ -e "${CROSS_COMPILERS_DIR}/${ARCH}" ]; then
    echo " The cross compiler is already installed ( directory : ${CROSS_COMPILERS_DIR}/${ARCH} )"
    bcont="YES"
    while [ "$bcont" = "YES" ] ; do
        echo " Do you want to replace it [y/n] ?"
        read rep
        if [ "$rep" = "y" ]; then bcont="NO"; elif [ "$rep" = "n" ]; then bcont="NO" ; fi
    done
    if [ "$rep" = "y" ]; then
        echo "Deleting ${CROSS_COMPILERS_DIR}/${ARCH} "
        rm -rf ${CROSS_COMPILERS_DIR}/${ARCH}
    fi
else
    rep="y"
    mkdir -p ${CROSS_COMPILERS_DIR}
fi

if [ "$rep" = "y" ]; then
    cd ${CROSS_COMPILERS_DIR}
    FILENAME=CROSS_COMPILER_${CC_DIR}.tar.bz2
    echo " *** Cross Compiler $ARCH target for $OS: Downloading "
    wget -c https://files.inria.fr/pacap/heptane/CROSS_COMPILERS/${FILENAME}


    if [ -e "${FILENAME}" ]; then
        echo " ***  Cross Compiler: Installing"
        tar xfj ${FILENAME}
        rm -f ${FILENAME}
        ln -s ${CC_DIR} ${ARCH}
    fi

    cd ..


    mkdir -p DSLITE
    cd DSLITE
    DSLITE_ROOT=$(pwd)
    echo "DSLITE ROOT IS ${DSLITE_ROOT}"
    echo " *** DSLITE tool for flashing the MSP430: Downloading "
    wget -O DSLITE.tar.xz -c "https://filesender.renater.fr/download.php?token=d1298dbd-e777-4a3c-b310-f304e174f359&files_ids=37514398"
    echo " *** DSLITE tool for flashing the MSP430: Installing "
    tar xf DSLITE.tar.xz
    cd ..

    echo "export PATH=\"\$PATH:${CROSS_COMPILERS_DIR}/${ARCH}/bin\":${DSLITE_ROOT}/DebugServer/bin" > env.sh
    chmod +x env.sh

    echo "Do you allow me to install TI udev rules in your udev rules.d folder ? (y/n)"
    read rep
    if [ "$rep" = "y" ]; then
        echo "** Executing TI udev install script at CROSS_COMPILERS/MSP430/install_scripts/msp430uif_install.sh"
        sudo ${CROSS_COMPILERS_DIR}/${ARCH}/install_scripts/msp430uif_install.sh --install
    else
        echo "You can install the rules manually. The rules are located in CROSS_COMPILERS/MSP430/instal_scripts"
    fi

    source ./env.sh

    echo "*** Checking installation ***"
    echo -n "MSP430 Compiler "
    msp430-elf-gcc -v &> /dev/null
    if [ $? -eq 0 ]; then
        echo "[OK]"
    else :
        echo "[ERROR]"
        msp430-elf-gcc -v
    fi

    echo -n "DSLITE Flasher "
    DSLite help &> /dev/null
    if [ $? -eq 0 ]; then
        echo "[OK]"
    else :
        echo "[ERROR]"
        DSLite help
    fi

    echo -n "UDEV Rules "
    ls "/etc/udev/rules.d/61-msp430uif.rules" &> /dev/null
    if [ $? -eq 0 ]; then
        echo "[OK]"
    else :
        echo "[???]"
    fi

    echo "*** DONE ***"
    echo "To remove any trace of this tutorial, remove this folder and the '61-msp430uif.rules' in '/etc/udev/rules.d/'"
    echo "To continue with this tutorial, run 'source env.sh'"
fi
