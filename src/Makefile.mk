GCC=msp430-elf-gcc
LD=msp430-elf-gcc
OBJDUMP=msp430-elf-objdump
TARGET=main.elf
# If you update the MCU, you need to update DSLITE_CONFIG as well
MCU=msp430fr5969

# Root folder containing CROSS_COMPILERS and DSLITE folders
ROOT=../..
COMPILER_ROOT=$(ROOT)/CROSS_COMPILERS/MSP430

# Compilation options
INCLUDES=-I$(COMPILER_ROOT)/include
LD_SCRIPT=-T $(COMPILER_ROOT)/include/$(MCU).ld -L $(COMPILER_ROOT)/include/
CFLAGS=-mmcu=${MCU}

# Flash configuration
DSLITE_ROOT=$(ROOT)/DSLITE/
DSLITE=$(DSLITE_ROOT)/DebugServer/bin/DSLite
DSLITE_CONFIG=$(DSLITE_ROOT)/MSP-EXP430FR5969LP.ccxml

#
DUMP=$(TARGET:.elf=.objdump)

all: $(TARGET) $(DUMP)

$(DUMP):$(TARGET)
	$(OBJDUMP) -S $< > $(DUMP)

$(TARGET): $(OBJS) $(LIBS)
	$(LD) $(CFLAGS) -o main.elf $(LD_SCRIPT) $(OBJS) $(LDFLAGS) $(INCLUDES)

%.o: %.c
	$(GCC) $(CFLAGS) $(INCLUDES) -c $<

flash: $(TARGET)
	${DSLITE} load -c ${DSLITE_CONFIG} $<

clean:
	rm -f *.o *.elf
