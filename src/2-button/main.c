#include "msp430.h"

// This code is required for the tutorial;
// I won't get into the details but do not remove this function
int setup() {

    WDTCTL = WDTPW | WDTHOLD; // Disable watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Unlock GPIO pins
}

int main() {
    // Do not remove this function
    setup();

    // Main loop
    while(1) {
    }
}

// Interrupt routine declaration
// Port 1 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
#else
#error Compiler not supported!
#endif
{
  // MISSION 2.2 Your interrupt routine here
}
